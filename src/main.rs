use anyhow::{anyhow, Context, Result};
use clap::{App, Arg, SubCommand};
use mdbook::book::{parse_summary, Link, SummaryItem};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{
    convert::TryFrom,
    env,
    fs::File,
    io::{BufReader, Read, Write},
    path::{Path, PathBuf},
    result::Result::Err,
};
use walkdir::WalkDir;

fn main() -> Result<()> {
    let opts = App::new("mdbook aggregator")
        .version("0.1")
        .about("generates SUMMARY.md from SUMMARY.yaml")
        .setting(clap::AppSettings::ArgsNegateSubcommands)
        .arg(
            Arg::with_name("folder")
                .index(1)
                .help("mdBook directory with src/SUMMARY.yaml inside"),
        )
        .subcommand(
            SubCommand::with_name("convert")
                .about("generate SUMMARY.yaml from SUMMARY.md")
                .arg(
                    Arg::with_name("file")
                        .index(1)
                        .help("path to SUMMARY.md")
                        .required(true),
                ),
        )
        .get_matches();

    if let ("convert", Some(args)) = opts.subcommand() {
        let path = args.value_of("file").unwrap();
        let mut summary_path = PathBuf::from(path);

        let mut f =
            File::open(&summary_path).context(format!("failed to open file {:?}", summary_path))?;
        let mut summary_text = String::new();
        f.read_to_string(&mut summary_text)
            .context(format!("failed to read {:?}", summary_path))?;

        let summary =
            parse_summary(&summary_text).context(format!("failed to parse {:?}", summary_path))?;

        let mut root_entries = Vec::new();

        for item in vec![
            summary.prefix_chapters,
            summary.numbered_chapters,
            summary.suffix_chapters,
        ]
        .drain(..)
        .flatten()
        {
            if let Some(entry) = Entry::from_summary_item(item)? {
                root_entries.push(entry);
            }
        }

        let summary = serde_yaml::to_string(&Entries(root_entries))?;

        summary_path.pop();
        summary_path.push("SUMMARY.yaml");
        let mut f =
            File::create(&summary_path).context(format!("failed to create {:?}", summary_path))?;
        f.write(summary.as_bytes())
            .context(format!("failed to write {:?}", summary_path))?;
    } else {
        match opts.value_of("folder") {
            Some(path) => {
                let mut path = PathBuf::from(path);
                path.push("src");
                env::set_current_dir(&path).context(format!("failed to enter {:?}", path))?;
            }
            None => {
                let path = Path::new("src");
                env::set_current_dir(path).context(format!("failed to enter {:?}", path))?;
            }
        }

        let mut summary_path = PathBuf::new();
        summary_path.push("./SUMMARY.yaml");

        let summary = OutSummary(
            summary_items_from_path(summary_path).context("failed to generate SUMMARY.md")?,
        )
        .try_to_string()?;

        let mut f = File::create("SUMMARY.md").context("failed to create SUMMARY.md")?;
        f.write(summary.as_bytes())
            .context("failed to write SUMMARY.md")?;
    }

    Ok(())
}

struct FmtSummaryItem(SummaryItem);

impl TryFrom<FmtSummaryItem> for Vec<String> {
    type Error = anyhow::Error;

    fn try_from(oe: FmtSummaryItem) -> Result<Self, Self::Error> {
        if let SummaryItem::Link(mut oe) = oe.0 {
            let mut lines = Vec::new();
            if let Some(path) = oe.location {
                lines.push(format!(
                    "- [{}]({})",
                    oe.name,
                    path.to_str()
                        .context(format!("Path {:?} is not valid unicode", path))?
                ));
            } else {
                lines.push(format!("- [{}]()", oe.name));
            }
            let mut child_lines = Vec::new();

            for child in oe.nested_items.drain(..) {
                child_lines.append(&mut Vec::<String>::try_from(FmtSummaryItem(child))?);
            }
            for line in child_lines.iter() {
                lines.push(format!("  {}", line));
            }
            return Ok(lines);
        }
        Ok(Vec::new())
    }
}

struct OutSummary(Vec<SummaryItem>);

impl OutSummary {
    pub fn try_to_string(mut self) -> Result<String> {
        let mut lines = Vec::new();
        lines.push(String::from("# Summary"));
        lines.push(String::from(""));
        for entry in self.0.drain(..) {
            lines.append(&mut Vec::<String>::try_from(FmtSummaryItem(entry))?);
        }
        let mut ret_val = String::new();
        for line in lines.iter() {
            ret_val.push_str(&format!("{}\n", line));
        }
        Ok(ret_val)
    }
}

fn prefix_summary_item_with_path(path: PathBuf, mut item: Link) -> Link {
    Link {
        name: item.name,
        location: item.location.map(|subpath| {
            let mut path = path.clone();
            path.push(subpath);
            path
        }),
        number: item.number,
        nested_items: item
            .nested_items
            .drain(..)
            .map(move |item| match item {
                SummaryItem::Link(item) => {
                    SummaryItem::Link(prefix_summary_item_with_path(path.clone(), item))
                }
                _ => item,
            })
            .collect(),
    }
}

fn summary_items_from_path(mut path: PathBuf) -> Result<Vec<SummaryItem>> {
    let extension = path.extension().context(format!(
        "failed to deserialize {:?}. no file extension",
        path
    ))?;
    match extension.to_str() {
        Some("yaml") => {
            let reader = {
                let f = File::open(&path).context(format!("failed to open {:?}", path))?;
                BufReader::new(f)
            };

            path.pop();

            let mut entries: Entries = serde_yaml::from_reader(reader)
                .context(format!("failed to deserialize {:?}", path))?;

            let mut out_entries = Vec::with_capacity(entries.0.len());

            for entry in entries.0.drain(..) {
                out_entries.push(entry.to_summary_item(path.clone())?);
            }

            Ok(out_entries)
        }
        Some(_) => {
            let mut f = File::open(&path).context(format!("failed to open file {:?}", path))?;
            let mut summary_text = String::new();
            f.read_to_string(&mut summary_text)
                .context(format!("failed to read {:?}", path))?;

            path.pop();

            let summary =
                parse_summary(&summary_text).context(format!("failed to parse file {:?}", path))?;

            let mut root_entries = Vec::new();

            for chapters in vec![
                summary.prefix_chapters,
                summary.numbered_chapters,
                summary.suffix_chapters,
            ] {
                for item in chapters {
                    if let SummaryItem::Link(item) = item {
                        let entry =
                            SummaryItem::Link(prefix_summary_item_with_path(path.clone(), item));
                        root_entries.push(entry);
                    }
                }
            }

            Ok(root_entries)
        }
        None => Err(anyhow!(format!(
            "failed to deserialize {:?}. Path is not valid unicode",
            path
        ))),
    }
}

#[derive(Serialize, Deserialize)]
struct Entries(Vec<Entry>);

#[derive(Serialize, Deserialize)]
struct Entry {
    name: String,
    #[serde(rename = "src")]
    source: String,
    #[serde(default)]
    #[serde(skip_serializing_if = "Children::skip")]
    children: Children,
}

impl Entry {
    fn to_summary_item(self, mut path: PathBuf) -> Result<SummaryItem> {
        let children = match self.children {
            Children::Child(subpath) => {
                let mut path = path.clone();
                path.push(subpath);
                summary_items_from_path(path)?
            }
            Children::Regex(regex_child) => {
                let regex =
                    Regex::new(regex_child.regex.as_str()).context("failed to compile regex")?;

                let path = match regex_child.path {
                    Some(_path) => {
                        let mut path = path.clone();
                        path.push(_path);
                        path
                    }
                    None => path.clone(),
                };

                let mut children = Vec::new();

                let iter = match regex_child.recursive {
                    Some(true) => WalkDir::new(path.as_path()).follow_links(true).into_iter(),
                    _ => WalkDir::new(path.as_path())
                        .follow_links(true)
                        .max_depth(1)
                        .into_iter(),
                };

                for entry in iter {
                    let entry = entry.context("can't access dir entry")?;

                    let file_name = entry
                        .file_name()
                        .to_str()
                        .context(format!("Path {:?} is not valid unicode", path))?
                        .to_string();

                    if entry.file_type().is_file() && regex.is_match(&file_name) {
                        children.push(SummaryItem::Link(Link {
                            name: slug::slugify(&file_name),
                            location: {
                                let mut path = path.clone();
                                path.push(file_name);
                                Some(path)
                            },
                            number: None,
                            nested_items: Vec::new(),
                        }));
                    }
                }

                children
            }
            Children::Entries(entries) => {
                let mut children = Vec::new();
                for entry in entries {
                    children.push(entry.to_summary_item(path.clone())?);
                }
                children
            }
        };

        path.push(self.source);

        Ok(SummaryItem::Link(Link {
            name: self.name,
            location: Some(path),
            number: None,
            nested_items: children,
        }))
    }

    fn from_summary_item(item: SummaryItem) -> Result<Option<Entry>> {
        match item {
            SummaryItem::Link(link) => {
                let mut children = Vec::new();
                for child in link.nested_items {
                    if let Some(entry) = Entry::from_summary_item(child)? {
                        children.push(entry);
                    }
                }

                Ok(Some(Entry {
                    name: link.name.to_string(),
                    source: link
                        .location
                        .as_ref()
                        .context("entry is missing source")?
                        .to_str()
                        .context(format!("Path {:?} is not valid unicode", &link.location))?
                        .to_string(),
                    children: Children::Entries(children),
                }))
            }
            _ => Ok(None),
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
enum Children {
    Regex(RegexChild),
    Entries(Vec<Entry>),
    Child(String),
}

impl Default for Children {
    fn default() -> Children {
        Children::Entries(Vec::new())
    }
}

impl Children {
    fn skip(&self) -> bool {
        match self {
            Children::Entries(v) => v.is_empty(),
            _ => false,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
struct RegexChild {
    pub regex: String,
    pub path: Option<String>,
    pub recursive: Option<bool>,
}

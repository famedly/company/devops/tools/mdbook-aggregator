FROM docker.io/rustlang/rust:nightly-slim AS builder
ADD . ./
RUN rustup toolchain add stable && rustup default stable
RUN cargo build --release && cargo build --release --package mdbook

FROM docker.io/alpine:edge
COPY --from=builder /target/release/mdbook-aggregator /usr/local/bin/mdbook-aggregator
COPY --from=builder /target/release/mdbook /usr/local/bin/mdbook
